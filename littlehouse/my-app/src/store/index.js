import { legacy_createStore, applyMiddleware , combineReducers } from 'redux'

import logger from 'redux-logger';//打印日志

import thunk from 'redux-thunk';

import Reducer from './modules/user';

const reducer=combineReducers({
    Reducer
})

export default legacy_createStore(reducer,applyMiddleware(thunk,logger))