const initialState = {
  List: [],
  archList: [],//
  viewList: [],
  detailList: {},//详情页
  rightList: [],//右边数据
  categoryList: [],//首页标签
  filterList: [],//筛选后的数据
  active: 0,//高亮
  setlist: [],//右侧标签
  searchList:[],//搜索后的数据
  likeFlag:false,
  likeLength:0,
  pageList:[],
  logoimg:"",
  commendList:[],
  num:0,
  tocList:[{level: "1", id: "欢迎使用-wipi-markdown-编辑器", text: "欢迎使用 Wipi Markdown 编辑器"},
  {level: "2", id: "什么是-markdown", text: "什么是 Markdown"},
  {level: "3", id: "1-待办事宜-todo-列表", text: "1. 待办事宜 Todo 列表"},
  {level: "3", id: "2-高亮一段代码code", text: "2. 高亮一段代码[^code]"},
  {level: "3", id: "3-绘制表格", text: "3. 绘制表格"},
  {level: "3", id: "4-嵌入网址", text: "4. 嵌入网址"}],

}



const Reducer = (state = initialState, { type, payload }) => {
  switch (type) {

    case "GET_COMMEND":
      state.commendList=payload.data[0]
      return {
        ...state,
        commendList: [...state.commendList],
      }
    case "GET_LIST":
      state.List = payload.data
      return {
        ...state,
        List: [...state.List],
      }
    case "GET_VIEW":
      state.viewList = payload.data[0]
      return {
        ...state,
        List: [...state.viewList]
      }
    case "GOTO_DETAIL":
      state.List.forEach((item) => {
        if (item.id === payload) {
          state.detailList = item
        }
      })
      return {
        ...state,
      }

    case "RIGHT_LIST":
      state.rightList = payload.data
      state.categoryList=[]
      payload.data.forEach((item,index)=>{
      const flag = state.categoryList.some(v => v === item.label)
        if(item.label){
          if (!flag) {
            state.categoryList.push(item.label)
        }
        }
      })
      state.categoryList.unshift('all')
      state.categoryList.forEach((item,index)=>{
        if(item==='ghhh888'){
          state.categoryList.splice(index,1)
        }
      })
      return {
        ...state,
        rightList: [...state.rightList],
        categoryList: [...state.categoryList],
      }

    case "BEGIN_FILTER_LIST":
      state.filterList = state.List.filter((item) => item.category ? item.category.label.includes(payload.item) : '')
      state.active = payload.index
      return {
        ...state,
        filterList: [...state.filterList]
      }

    case "FILTER_LIST":
      state.filterList = state.List.filter((item) => item.category ? item.category.label.includes(payload) : '')
      return {
        ...state,
        filterList: [...state.filterList]
      }
    case "SEARCH_LIST":
      state.searchList = state.List.filter((item) => item.title.includes(payload))
      return {
        ...state,
        searchList: [...state.searchList]
      }
  case "SET_FILT":
    state.setlist = payload.data;
    state.num=0
    for(let i=0;i<state.setlist.length;i++){
      for(let j=0;j<state.List.length;j++){
        if(state.setlist[i].label===state.List[j].label){
          state.num++
        }
      }
    }
    return {
      ...state,
      setlist: [...state.setlist],
      num:state.num
    }

  case "TAG_FILTER":
    state.filterList=[]
    state.List.forEach((item, index) => {
      var a = 0;
      for (let i = 0; i < item.tags.length; i++) {
        if (item.tags[i].label === payload) {
          a = 1
        }
      }
      if (a === 1) {
        state.filterList.push(item)
      }
    })
    return {
      ...state,
      filterList: [...state.filterList]
    }

  case "SEARCH_LIST":
    state.searchList=state.List.filter((item)=>item.title.includes(payload))
    return {
      ...state,
      searchList:[...state.searchList]
    }
    case "DETAIL_LIST":
    state.likeFlag=state.detailList.flag=!state.detailList.flag
    if(state.likeFlag){
      state.detailList.likes++
      state.likeLength=state.detailList.likes
    }
    else{
      state.detailList.likes--
      state.likeLength=state.detailList.likes
    }
    return {
      ...state,
      detailList:state.detailList,
      likeLength:state.likeLength,
    }
  case "LIST_LIKE":
    state.List.forEach(item=>{
      if(item.id===payload){
        state.likeFlag=item.flag=!item.flag
        if(item.flag){
          item.likes++
          state.likeLength=item.likes
        }
        else{
          item.likes--
          state.likeLength=item.likes
        }
      }
    })
    return {
      ...state,
      List:[...state.List],
    }

  case "CHANGE_FLAG":
    state.commendList.forEach(item=>{
      if(item.id===payload){
        item.flag=!item.flag
      }
    })
    return {
      ...state,
      commendList:[...state.commendList],
    }

  case "CHILDREN_FLAG":
    state.commendList.forEach(item=>{
      if(item.id===payload.itemid){
        if(item.children){
          console.log(item);
          item.children.forEach(v=>{
            if(v.id===payload.vid){
              v.flag=!v.flag
            }
          })
        }
      }
    })
    return {
      ...state,
      commendList:[...state.commendList],
    }


  case "GET_LOGO":
    state.logoimg=payload.systemFavicon
    return{
      ...state,
      logoimg:state.logoimg
    }

  case "ADD_COMMENDLIST":
    state.commendList.push({
      children: [],
      content: payload,
      createAt: "2022-09-23T12:46:54.270Z",
      email: "2482833780@qq.com",
      flag: false,
      hostId: "5ad3fe5a-a302-4c3b-a334-b0bb58628aa1",
      html: "<p>Vue</p>\n",
      id: +new Date(),
      name: "112",
      parentCommentId: null,
      pass: true,
      replyUserEmail: null,
      replyUserName: null,
      updateAt: "2022-09-23T13:07:03.000Z",
      url: "/article/5ad3fe5a-a302-4c3b-a334-b0bb58628aa1",
      userAgent: "Edge 105.0.1343.42 Blink 105.0.0.0 Windows 10",
    })
    return{
      ...state,
      commendList:[...state.commendList]
    }
  case "ADD_COMMENDLISTCHILDREN":
    state.commendList.forEach(item=>{
      if(item.id===payload.id){
        item.children.push({
          content: payload.value,
          createAt: "2022-09-23T12:46:54.270Z",
          email: "2482833780@qq.com",
          flag: false,
          hostId: "5ad3fe5a-a302-4c3b-a334-b0bb58628aa1",
          html: "<p>Vue</p>\n",
          id: +new Date(),
          name: "112",
          parentCommentId: null,
          pass: true,
          replyUserEmail: null,
          replyUserName: null,
          updateAt: "2022-09-23T13:07:03.000Z",
          url: "/article/5ad3fe5a-a302-4c3b-a334-b0bb58628aa1",
          userAgent: "Edge 105.0.1343.42 Blink 105.0.0.0 Windows 10",
        })
      }
    })
    return{
      ...state,
      commendList:[...state.commendList]
    }
  case "ADD_COMMEND":
    state.commendList.forEach(item=>{
      if(item.id===payload.id){
        item.children.push({
          content: payload.value,
          createAt: "2022-09-23T12:46:54.270Z",
          email: "2482833780@qq.com",
          flag: false,
          hostId: "5ad3fe5a-a302-4c3b-a334-b0bb58628aa1",
          html: "<p>Vue</p>\n",
          id: +new Date(),
          name: "112",
          parentCommentId: null,
          pass: true,
          replyUserEmail: null,
          replyUserName: null,
          updateAt: "2022-09-23T13:07:03.000Z",
          url: "/article/5ad3fe5a-a302-4c3b-a334-b0bb58628aa1",
          userAgent: "Edge 105.0.1343.42 Blink 105.0.0.0 Windows 10",
        })
      }
    })
    return{
      ...state,
      commendList:[...state.commendList]
    }


  default:
    return{
      ...state
    }
  }
}



export default Reducer;