import React,{ useContext  } from 'react'


const AppContext=React.createContext()

const Message=()=>{
    const {username}=useContext(AppContext)
    return(
        <div>
            <p> 我是message组件 </p>
            <p> 名字{username} </p>
        </div>
    )
}


function Classify(){


    return(
        <AppContext.Provider value={{
            username:"毛冰松"
        }}>
            <Message></Message>
        </AppContext.Provider>
    )
}

export default  Classify;