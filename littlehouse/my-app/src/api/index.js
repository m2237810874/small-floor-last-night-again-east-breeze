import axios from "../utils/request";

export const get_list=()=>{
    return (dispatch)=>{
        axios.get('/api/api/article/recommend?articleId=c92874d5-96ec-4d18-b0f3-b457e214a791').then(res=>{
            dispatch({
                type:'GET_LIST',
                payload:res.data
            })
        })

    }
}
export const set_list=()=>{
    return (dispatch)=>{
        axios.get('/api/api/tag?articleStatus=publish').then(res=>{
            dispatch({
                type:'SET_FILT',
                payload:res.data
            })
        })
    }
}

export const get_rightlist=()=>{
    return (dispatch)=>{
        axios.get('/api/api/category').then(res=>{
            dispatch({
                type:'RIGHT_LIST',
                payload:res.data
            })
        })
    }
}
export const get_view=()=>{
    return (dispatch)=>{
        axios.get('/api/api/knowledge').then(res=>{
            dispatch({
                type:'GET_VIEW',
                payload:res.data
            })
        })
    }
}

export const get_logo=()=>{
    return (dispatch)=>{
        axios.post('/api/api/setting/get').then(res=>{
            console.log(res)
            dispatch({
                type:'GET_LOGO',
                payload:res.data.data
            })
        })
    }
}


export const get_commend=(id)=>{
    return (dispatch)=>{
        // https://creationapi.shbwyz.com/api/comment/host/5ad3fe5a-a302-4c3b-a334-b0bb58628aa1
        axios.get('https://creationapi.shbwyz.com/api/comment/host/5ad3fe5a-a302-4c3b-a334-b0bb58628aa1').then(res=>{
            dispatch({
                type:'GET_COMMEND',
                payload:res.data
            })
        })
    }
}

export const goto_detail=(id)=>{
    return({
        type:'GOTO_DETAIL',
        payload:id
    })
  };
export const filter_list=(item)=>{
    return({
        type:'FILTER_LIST',
        payload:item
    })
  };
export const begin_filter_list=(item,index)=>{
    return({
        type:'BEGIN_FILTER_LIST',
        payload:{item,index}
    })
  };

export const tag_filter_list=(label)=>{
    return({
        type:"TAG_FILTER",
        payload:label
    })
}

export const search_list=(value)=>{
    return({
        type:'SEARCH_LIST',
        payload:value
    })
  };
  export const detailList_like=()=>{
    return({
        type:'DETAIL_LIST',
    })
  };
export const list_like=(id)=>{
    return({
        type:'LIST_LIKE',
        payload:id
    })

  };
export const change_flag=(id)=>{
    return({
        type:'CHANGE_FLAG',
        payload:id
    })
  };
export const children_flag=(itemid,vid)=>{
    return({
        type:'CHILDREN_FLAG',
        payload:{itemid,vid}
    })
  };
export const add_commendList=(value)=>{
    return({
        type:'ADD_COMMENDLIST',
        payload:value
    })
  };
export const add_commendListChildren=(value,id)=>{
    return({
        type:'ADD_COMMENDLISTCHILDREN',
        payload:{value,id}
    })
  };
export const add_commend=(value,id)=>{
    return({
        type:'ADD_COMMEND',
        payload:{value,id}
    })
  };

export const change_pay=(id)=>{
    return({
        type:'CHANGE_PAY',
        payload:id
    })
}
