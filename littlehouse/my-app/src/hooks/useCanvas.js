import { useState , useEffect } from 'react'

// const getImg=(url)=>{
//     return new Promise ((resolve,reject)=>{
//         //实例化image
//         const img=new Image();
//         img.crossOrigin='anonymous'//允许跨域
//         img.onload=()=>{
//             resolve(img)
//         }
//         img.onerror=(error)=>{
//             reject(error)
//         };
//         img.src=url;
//     })
// }

const drawUrl=async(ctx,data,canvasDom)=>{
    console.log(data)
    //绘制填充背景
    ctx.fillStyle='#fff';
    ctx.fillRect(0,0,400,679);
    //绘制标题
    ctx.font='30px pink';
    ctx.fillStyle='#000';
    ctx.fillText(data.title, 124 , 30 )

    //绘制标题下面的线
    ctx.beginPath();
    ctx.strokeStyle='blue';
    ctx.moveTo(0,40);
    ctx.lineTo(400,40)

    //开始绘制
    ctx.stroke();

    //绘制图片
    // const img=getImg(data.cover)
    // console.log(img)
    // ctx.drawImage(img,50,100,200,200);

    //绘制下面的文字
    ctx.font='12px yellow';
    ctx.fillStyle='#000';
    ctx.fillText(data.title,50,450);
    ctx.fillText(data.summary,50,470);

    //生程url 把canvas转成图片
    const imgUrl=canvasDom.current.toDataURL();
    return imgUrl;

};

//生成canvas
const createCanvas=async (data,canvasDom)=>{
    //设置canvas的宽高
    //canvasDom
    canvasDom.current.width=400;
    canvasDom.current.height=679;
    //创建上下文对象
    const ctx=canvasDom.current.getContext('2d');
    //绘制
    let imgUrl=await drawUrl(ctx,data,canvasDom);
    return imgUrl
}

const useCanvas=(data,canvasDom)=>{
    const [ url , setUrl ] =useState('')
    useEffect(()=>{
        (async()=>{
            let url=await createCanvas(data,canvasDom);
            setUrl(url);
        })();
    },[data]);
    return url
}

export default useCanvas