//实时获取视口的大小 封装自定义hooks
//自定义hooks 必须 以use开头

import {useCallback, useEffect, useState} from "react";


export const useWinSize=()=>{
    const [size,setSize]=useState({
        width:document.documentElement.clientWidth,
        height:document.documentElement.clientHeight
    })
    // 使用useCallback
    const changeSize=useCallback(()=>{
       setSize({
        width:document.documentElement.clientWidth,
        height:document.documentElement.clientHeight
       }) 
    },[])
    useEffect(()=>{
        // 绑定页面监听  组件销毁的时候 清除绑定
        window.addEventListener('resize',changeSize)
        return ()=>{
            window.removeEventListener('resize',changeSize)
        }
    },[])
    // 使用useEffect在组件创建时监听resize事件  resize时重新设置state
    return size
}