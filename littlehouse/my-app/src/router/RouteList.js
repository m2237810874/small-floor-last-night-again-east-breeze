import { lazy } from 'react'

const routes = [
    {
        path: '/home',
        component: lazy(() => import('../views/Home/Index')),
        children: [
            {
                path: "/home/articles",
                component: lazy(() => import('../views/Homes/Articles'))
            },
            {
                path: "/home/archives",
                component: lazy(() => import('../views/Homes/Archives'))
            },
            {
                path: "/home/knowledgebooks",
                component: lazy(() => import('../views/Homes/Knowledgebooks')),
            },
            {
                path:'/home/detail/:id',
                component:lazy(()=>import('../views/Homes/Detail/Index'))
            },
            {
                path:'/home/category/:id',
                component:lazy(()=>import('../views/Homes/Category/Index'))
            },
            {
                path:"/home/tag/:id",
                component:lazy(()=>import('../views/Homes/FilterDetil/Index'))
            },
            {
                path: "/home/knowledgebooks/detail/:id",
                component: lazy(() => import('../views/Homes/KnowledgeDetail')),
            },
        ]
    },
    {
        path:'/try',
        component:lazy(()=>import("../views/try/Index"))
    },
    {
        path:'/search',
        component:lazy(()=>import("../views/Search/Index"))
    },
    {
        path: "/",
        redirect: "/home/articles"
    },
    {
        path:'/context',
        component:lazy(()=>import("../pages/classify"))
    }
    
]

export default routes