import routes from './RouteList';

import { Suspense } from 'react'

import { Navigate ,
         Route , //配置路由信息的组件
         Routes , 
         BrowserRouter as Router //采用history模式监视全局路由变化 
        } from 'react-router-dom'

function RouterViews(){

    //封装路由渲染的方法
    const renderRoute=(routes)=>{
        return routes.map((item,index)=>{
            return <Route
                    key={index}
                    path={item.path}
                    element={item.redirect?<Navigate to={item.redirect}></Navigate> : <item.component/>}
                    >
                        {
                            item.children&&renderRoute(item.children)
                        }
                    </Route>
        })
    }

    return(
        <Suspense fallback="loading...">
            <Router>
                <Routes>
                    {
                        renderRoute(routes)
                    }
                </Routes>
            </Router>
        </Suspense>
    )


}

export default RouterViews