const Mock=require('mockjs')

const MockData=Mock.mock({
    "list|5":[

        {
            "title":"@ctitle(3,5)",
            "id":"@id",
            "img":"@image(100X100,@color)",
            "watch":0,
            'date':'@date',
        }
    ],
    'rightlist|8':[
        {
            "id":"@id",
            "title":"@word(3,5)",
            // "text":"@word",
            // "num":0
        }
    ]
})

module.exports = MockData
