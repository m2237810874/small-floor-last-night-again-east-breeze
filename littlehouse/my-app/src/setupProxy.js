const {createProxyMiddleware} =require("http-proxy-middleware")

module.exports=(app)=>{
    app.use(
        '/api',
        createProxyMiddleware({
            target:"https://creationapi.shbwyz.com",
            changeOrigin:true,
            pathRewrite(path){
                return path.replace('/api','')
            }
        })
    )
    app.use(
        '/payment',
        createProxyMiddleware({
            target:"http://localhost:9000",
            changeOrigin:true,
            pathRewrite(path){
                return path.replace('/payment','')
            }
        })
    )
}