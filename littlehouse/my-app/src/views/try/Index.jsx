import React,{useState,useEffect} from 'react'
import "./index.scss"
import style from '../../style/style.module.css'
import Phone from "../../component/Phone/Index"
const isLight=()=>{
    const currentDate=new Date();
    return currentDate.getDate() > 6 && currentDate.getHours() < 19;
}

const Index = () => {
    const [theme,setTheme]=useState(()=>(isLight()?'白':'黑'));
    useEffect(()=>{
        document.documentElement.className = theme ==="白" ? "dark" : "lint"                    
    },[theme])
    return (
        <div className={style.app}>
            <div className="tryTop">
                <button onClick={()=>{
                    setTheme(theme==='白'?'黑':'白')
                }}>
                    主题
                </button>

            </div>
            <div className="trymain">
               <Phone>
                111
               </Phone>
            </div>
            <div className="tryfooter">
                  
            </div>
        </div>
  )
}

export default Index