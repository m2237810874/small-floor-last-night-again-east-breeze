import React,{useEffect, useState} from 'react'
import "./index.scss"
import { useDispatch, useSelector } from 'react-redux'
import Right from "../../../component/right"
import Alert from '../../../component/Alert/Alert';
import * as api from '../../../api/index'
import { Empty } from 'antd';
import { EyeOutlined , ApiOutlined , HeartOutlined} from '@ant-design/icons';
import style from '../../../style/style.module.css'
import { useWinSize } from '../../../hooks/WinSize'
import { useNavigate } from 'react-router-dom';



const Index = () => {
  const dispatch=useDispatch()
  const navigate=useNavigate()
  const size=useWinSize()
  useEffect(()=>{
    dispatch(api.get_view())
  },[dispatch])
  const viewList=useSelector((state)=>state.Reducer.viewList)
  console.log(viewList);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [alertitem,SetItem]=useState({})
  const close=(flag)=>{
    setIsModalOpen(flag)
  }
  return (
    <div className='three'>
      {
          size.width>=700?
          <div className="knowpc">
          <div className='zuo' id={style.nav} style={{width:"71%"}}>
              {
                viewList.length?viewList.map((item,index)=>{
                  return  <div key={index} className="three_list" onClick={()=>navigate("/home/knowledgebooks/detail/"+item.id)}>
                            <div className="three_list_item">
                              <h4 id={style.hcolor}>
                                {
                                  item.title
                                }
                              </h4>
                              <p>
                                <span>
                                  大约...分钟前
                                </span>
                                <span>
                                  {
                                    item.classify
                                  }
                                </span>
                              </p>

                            </div>
                            <div className="three_list_con" id={style.nav}>
                              <div className="three_list_con_left">
                                  {item.cover?<img width='100px' height='100px' src={item.cover} alt="" />:''}
                              </div>
                              <div className="three_list_con_right">
                                  <p>{item.summary}</p>
                                  <p>
                                    <span>
                                      <EyeOutlined />
                                      {item.watch}
                                    </span>
                                    <b>
                                      .
                                    </b>
                                    <span className='fx' onClick={()=>{
                                      setIsModalOpen(true)
                                      SetItem(item)
                                    }}>
                                      <ApiOutlined />
                                      share
                                    </span>
                                  </p>
                              </div>
                            </div>
                        </div>
                }):<Empty />
              }
          </div>
          <div className="you">
            <Right></Right>
          </div>
          </div>
          :
          <div className="knowpc">
          <div className='zuo' id={style.nav} style={{width:"100%"}}>
              {
                viewList.length?viewList.map((item,index)=>{
                  return  <div key={index} className="three_list">
                            <div className="three_list_item">
                              <h4 id={style.hcolor}>
                                {
                                  item.title
                                }
                              </h4>
                              <p>
                                <span>
                                  大约...分钟前
                                </span>
                                <span>
                                  {
                                    item.classify
                                  }
                                </span>
                              </p>

                            </div>
                            <div className="three_list_con">
                              <div className="three_list_con_left">
                                  {item.cover?<img width='100px' height='100px' src={item.cover} alt="" />:''}
                              </div>
                              <div className="three_list_con_right">
                                  <p>{item.summary}</p>
                                  <p>
                                    <span>
                                      <HeartOutlined />
                                      {item.like}
                                      ·
                                    </span>
                                    <span>
                                      <EyeOutlined />
                                      {item.watch}
                                      ·
                                    </span>
                                    <span className='fx' onClick={()=>{
                                      setIsModalOpen(true)
                                      SetItem(item)
                                    }}>
                                      <ApiOutlined />
                                      share
                                    </span>
                                  </p>
                              </div>
                            </div>
                        </div>
                }):<Empty />
              }
          </div>
          </div>
      }
      <Alert isModalOpen={isModalOpen} close={close} alertitem={alertitem}/>
    </div>
  )
}

export default Index