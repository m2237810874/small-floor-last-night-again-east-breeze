import React,{useState} from 'react'
import './index.scss'
import {useParams} from 'react-router-dom'
import { useSelector } from 'react-redux'
import { Button } from 'antd';
import style from '../../../style/style.module.css'
import { useNavigate } from 'react-router-dom';
import { EyeOutlined , ApiOutlined } from '@ant-design/icons';
import Alert from '../../../component/Alert/Alert';

const  Index = () => {
  const params=useParams()
  const navigate=useNavigate()
  console.log(params.id)
  const viewList=useSelector((store)=>store.Reducer.viewList)
  const nowList=useSelector((store)=>store.Reducer.viewList.filter((item=>item.id===params.id)))
  console.log(nowList)
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [alertitem,SetItem]=useState({})
  const close=(flag)=>{
    setIsModalOpen(flag)
  }
  return (
    <div className='know_detail'>
      <div className='know_detail_top'>
        <p>知识小测  /  {nowList[0].title}</p>
        <h2 id={style.hcolor}>{nowList[0].title}</h2>
      </div>
      <div className='know_main'>
          <div className='know_main_left' id={style.nav}>
            <img style={{width:"100%",height:"500px"}} src={nowList[0].cover} alt="" />
            <p>{nowList[0].title}</p>
            <p>{nowList[0].summary}</p>
            <p>{nowList[0].views}次阅读{nowList[0].createAt}</p>
            <p>
                <Button type="primary" danger disabled>
                    开始阅读
                </Button>
            </p>
          </div>
          <div className='know_main_right' id={style.dark}>
            <div className="know_insed" id={style.nav}>
            <h3 id={style.nav}>其他阅读笔记</h3>
              <ul>
                {
                  viewList.map((item,index) => {
                      if(item.id!==params.id){
                        return  <div key={index} className="konw_list" id={style.nav} onClick={()=>navigate("/home/knowledgebooks/detail/"+item.id)}>
                        <div className="three_list_item" style={{marginTop:"8px"}}>
                          <h4 id={style.hcolor}>
                            {
                              item.title
                            }
                          </h4>
                          <p>
                            <span style={{marginLeft:'10px'}}>
                              大约...分钟前
                            </span>
                            <span>
                              {
                                item.classify
                              }
                            </span>
                          </p>

                        </div>
                        <div className="three_list_con"  id={style.nav}>
                          <div className="three_list_con_left">
                              {item.cover?<img width='100px' height='100px' src={item.cover} alt="" />:''}
                          </div>
                          <div className="three_list_con_right" id={style.nav}>
                              <p>{item.summary}</p>
                              <p>
                                <span>
                                  <EyeOutlined />
                                  {item.watch}
                                </span>
                                <b>
                                  .
                                </b>
                                <span className='fx' onClick={()=>{
                                  setIsModalOpen(true)
                                  SetItem(item)
                                }}>
                                  <ApiOutlined />
                                  share
                                </span>
                              </p>
                          </div>
                        </div>
                    </div>
                      }
                  })
                }
              </ul>
              </div>
          </div>
      </div>
      <Alert isModalOpen={isModalOpen} close={close} alertitem={alertitem}/>
    </div>
  )
}

export default Index