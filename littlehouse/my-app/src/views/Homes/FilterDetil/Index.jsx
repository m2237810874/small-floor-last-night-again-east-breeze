import { Button } from 'antd';
import React,{useState} from 'react'
import "./index.scss"
import * as api from '../../../api/index'
import { useParams } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import { HeartOutlined, EyeOutlined, ApiOutlined } from "@ant-design/icons";
import Alert from '../../../component/Alert/Alert';
import moment from 'moment';

const Index = () => {
    const params = useParams()
    const navigate = useNavigate()
    const dispatch = useDispatch()
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [alertitem, SetItem] = useState({});
    const taglist = useSelector((state) => state.Reducer.setlist)
    const filterlist = useSelector((state) => state.Reducer.filterList)
    const close = (flag) => {
        setIsModalOpen(flag);
      };
    return (
        <div className='filterdeil'>
            <div className="filterdeiltop">
                <div>
                    yu
                    <b>{params.id}</b>
                    tagRelativeArticles
                </div>
            </div>
            <div className='filterdeilmain'>
                <div className="maintop">
                    {
                        taglist.map((item, index) => {
                            return <Button
                                key={index}
                                onClick={() => {
                                    dispatch(api.tag_filter_list(item.label))
                                    navigate('/home/tag/' + item.label)

                                }}
                            >
                                {
                                    item.label
                                }
                            </Button>
                        })
                    }
                </div>
                <div className="mainconter">
                    {filterlist.map((item, index) => {
                        return (
                            <div key={index} className="item" onClick={() => navigate('/home/detail/' + item.id)}>
                                <div className="itemhead">
                                    <h4>{item.title}</h4>
                                    <p>
                                        <span>大约&ensp;{moment(item.createAt).startOf('hour').fromNow()}</span>
                                        <span>{item.category ? item.category.label : ''}</span>
                                    </p>
                                </div>
                                <div className="itemcon">
                                    <div className="itemlefy">
                                        {item.cover ? <img src={item.cover} alt="" /> : ""}
                                    </div>
                                    <div className="ltemright">
                                        <p>{item.desc}</p>
                                        <p>
                                            <span>
                                                <HeartOutlined />
                                                {item.likes}·
                                            </span>
                                            <span>
                                                <EyeOutlined />
                                                {item.views}·
                                            </span>
                                            <span
                                                className="fx"
                                                onClick={(e) => {
                                                    setIsModalOpen(true);
                                                    SetItem(item);
                                                    e.stopPropagation()
                                                }}
                                            >
                                                <ApiOutlined />
                                                share
                                            </span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        );
                    })}
                    <Alert isModalOpen={isModalOpen} close={close} alertitem={alertitem} />
                </div>
            </div>
        </div>
    )
}

export default Index