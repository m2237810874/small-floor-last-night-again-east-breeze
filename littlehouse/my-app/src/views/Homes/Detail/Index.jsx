import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import * as api from "../../../api/index";
import "./index.scss";
import "./markdown.scss";
import "./var.scss";
import Alert from "../../../component/Alert/Alert";
import { Badge } from "antd";
import style from "../../../style/style.module.css";
import Detailleft from "../../../component/deatilleft/Index";
import {
  ShareAltOutlined,
  HeartFilled,
  HeartOutlined,
} from "@ant-design/icons";
import PayAlert from "../../../component/PayAlert";
import { Sticky } from "react-vant";
import Detailright from '../../../component/detailright/Index'
const Index = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const params = useParams();
  useEffect(() => {
    dispatch(api.goto_detail(params.id));
    dispatch(api.get_commend(params.id));
  }, [dispatch]);
  const close = (flag) => {
    setIsModalOpen(flag);
  };
  const a = document.getElementById("欢迎使用-wipi-markdown-编辑器");
  const b = document.getElementById("什么是-markdown");
  const c = document.getElementById("1-待办事宜-todo-列表");
  const d = document.getElementById("2-高亮一段代码code");
  const e = document.getElementById("3-绘制表格");
  const f = document.getElementById("4-嵌入网址");
  const arr = [a, b, c, d, e, f];
  const [active, setActive] = useState(0);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [alertitem, SetItem] = useState({});
  const [pageSize, setpageSize] = useState(1);
  const [limit] = useState(5);
  const detailList = useSelector((state) => state.Reducer.detailList);
  const likeFlag = useSelector((state) => state.Reducer.likeFlag);
  const likeLength = useSelector((state) => state.Reducer.likeLength);
  const commendList = useSelector((state) => state.Reducer.commendList);
  const pageList = commendList.slice((pageSize - 1) * limit, pageSize * limit);
  const List = useSelector((state) => state.Reducer.List);
  const tocList = useSelector((state) => state.Reducer.tocList);
  const [value, setValue] = useState("");
  const [value1, setValue1] = useState("");
  const [value2, setValue2] = useState("");
  window.onscroll = function () {
    arr.forEach((item, index) => {
      if (document.documentElement.scrollTop >= item.offsetTop + 80) {
        setActive(index);
      }
    });
  };

  return (
    <div className="detail">
      <Detailleft
        detailList={detailList}
        pageSize={pageSize}
        setpageSize={setpageSize}
        pageList={pageList}
        value={value}
        setValue={setValue}
        value1={value1}
        setValue1={setValue1}
        value2={value2}
        setValue2={setValue2}
        dispatch={dispatch}
        commendList={commendList}
        limit={limit}
      ></Detailleft>
      <Sticky>
        <Detailright navigate={navigate}active={active} List={List} tocList={tocList} setActive={setActive} arr={arr}></Detailright>
      </Sticky>
      <div className="position" id={style.nav}>
        <div
          onClick={() => {
            dispatch(api.detailList_like());
          }}
        >
          <Badge count={likeFlag ? likeLength : detailList.likes}>
            {likeFlag ? (
              <HeartFilled style={{ color: "pink" }} />
            ) : (
              <HeartOutlined />
            )}
          </Badge>
        </div>
        <div onClick={()=>{
          const bottom=document.querySelectorAll('.detail_left .bottom')
          document.documentElement.scrollTop=bottom[0].offsetTop+100
        }}>
        <svg t="1664076739529" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="17000" width="30" height="30"><path d="M512 157.27104c104.35072 0 201.79968 31.89248 274.39616 89.8048 67.89632 54.16448 105.2928 124.75392 105.2928 198.75328s-37.39136 144.5888-105.2928 198.75328c-72.59648 57.91232-170.04544 89.8048-274.39616 89.8048-49.34656 0-98.85696 36.84352-156.18048 79.50848-9.53856 7.09632-21.55008 16.04096-33.5616 24.59136l0.04096-8.35072c0.384-63.60576 0.77824-129.37216-50.2016-161.19808-88.832-55.45472-139.78112-136.77568-139.78112-223.10912 0-74.00448 37.39136-144.5888 105.2928-198.75328 72.59136-57.9072 170.04032-89.8048 274.39104-89.8048m0-70.87104c-248.83712 0-450.56 160.92672-450.56 359.43424 0 115.03104 67.7376 217.44128 173.12768 283.23328 36.55168 22.81472-4.85376 194.55488 39.5008 207.55968 2.25792 0.66048 4.69504 0.97792 7.31136 0.97792 53.52448 0 178.53952-132.33152 230.62528-132.33152 248.83712 0 450.56-160.92672 450.56-359.43424C962.56 247.3216 760.83712 86.4 512 86.4z" fill="#2c2c2c" p-id="17001" data-spm-anchor-id="a313x.7781069.0.i4" class="selected"></path><path d="M660.48 414.09024H363.52a35.84 35.84 0 1 1 0-71.68h296.96a35.84 35.84 0 1 1 0 71.68zM614.4 577.93024H409.6a35.84 35.84 0 1 1 0-71.68h204.8a35.84 35.84 0 1 1 0 71.68z" fill="#2c2c2c" p-id="17002" data-spm-anchor-id="a313x.7781069.0.i5" class="selected"></path></svg>
        </div>
        <div>
          <ShareAltOutlined
            onClick={() => {
              setIsModalOpen(true);
              SetItem(detailList);
            }}
          />
        </div>
      </div>
      <Alert isModalOpen={isModalOpen} close={close} alertitem={alertitem} />
      {<PayAlert isPay={detailList.isRecommended} id={detailList.id} />}
    </div>
  );
};

export default Index;
