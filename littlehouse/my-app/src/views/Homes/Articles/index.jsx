import { Carousel } from "antd";
import React, { useEffect, useState } from "react";
import "./index.scss";
import { useSelector, useDispatch } from "react-redux";
import * as api from "../../../api/index";
import {
  HeartOutlined,
  EyeOutlined,
  ApiOutlined,
  HeartFilled,
} from "@ant-design/icons";
import Alert from "../../../component/Alert/Alert";
import FilterRight from "../../../component/FilterRight/FilterRight";
import { useNavigate } from "react-router-dom";
import { BackTop } from "antd";
import style from "../../../style/style.module.css";
import { useWinSize } from "../../../hooks/WinSize";
import Ipad from "../../../component/ipad/Index";
import moment from "moment";
import { Empty } from 'antd'


const contentStyle = {
  height: "314px",
  color: "#fff",
  lineHeight: "314px",
  textAlign: "center",
  background: "#364d79",
  position: "relative",
};
const Index = () => {
  const dispahch = useDispatch();
  const navigate = useNavigate();
  const size = useWinSize();
  const [active, SetActive] = useState(0);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [alertitem, SetItem] = useState({});
  useEffect(() => {
    dispahch(api.get_list());
    dispahch(api.get_rightlist());
  }, [dispahch]);
  const { List, categoryList } = useSelector(({ Reducer }) => {
    return {
      ...Reducer,
    };
  });
  const close = (flag) => {
    setIsModalOpen(flag);
  };
  const onChange = (currentSlide) => {};
  return (
    <div className="contener">
      {size.width >= 700 ? (
        <div className="contenterout">
          <div
            className="contenerleft"
            style={{ width: "71%", marginLeft: "3%" }}
          >
            <div className="banner">
              {/* 轮播图 */}
              <Carousel afterChange={onChange} autoplay="true">
                {List.map((item, index) => {
                  return item.cover ? (
                    <div key={index}>
                      <h3 style={contentStyle} height="314px">
                        <img
                          style={{
                            height: "100%",
                            width: "100%",
                            objectFit: "cover",
                          }}
                          src={item.cover}
                          alt=""
                        />
                        <div
                          style={{
                            width: "100%",
                            position: "absolute",
                            top: "0",
                            color: "#fff",
                            fontSize: "25px",
                          }}
                        >
                          <p>{item.title}</p>
                        </div>
                      </h3>
                    </div>
                  ) : (
                    ""
                  );
                })}
              </Carousel>
            </div>

            <div className="tab" id={style.dark}>
              {/* tab切换顶部标签 */}
              <div className="tabnav" id={style.tabnav}>
                <ul>
                  {categoryList.length?categoryList.map((item, index) => {
                    return (
                      <li
                        key={index}
                        className={active === index ? "active" : "navtop"}
                        onClick={() => {
                          // 控制高亮
                          SetActive(index);
                          if (item !== "all") {
                            //跳转路由
                            navigate("/home/category/" + item);
                            // 派发仓库
                            dispahch(api.begin_filter_list(item, index));
                          }
                        }}
                      >
                        {item}
                      </li>
                    );
                  })
                  :<div>loading...</div>
                }
                </ul>
              </div>
              <div className="tabcon" id={style.nav}>
                {/* 标签下的内容 */}
                {List.length?List.map((item, index) => {
                  return (
                    <div
                      key={index}
                      className="item"
                      onClick={() => navigate("/home/detail/" + item.id)}
                    >
                      <div className="itemhead" id={style.nav}>
                        <h4
                          style={{ fontSize: "16px" }}
                          className="hh"
                          id={style.hcolor}
                        >
                          {item.title}
                        </h4>
                        <p>
                          <span>大约&ensp;{moment(item.createAt).startOf('day').fromNow()}</span>
                          <span>
                            {item.category ? item.category.label : ""}
                          </span>
                        </p>
                      </div>
                      <div className="itemcon">
                        <div className="itemlefy">
                          {item.cover ? <img src={item.cover} alt="" /> : ""}
                        </div>
                        <div
                          className="ltemright"
                          style={{ justifyContent: "flex-end" }}
                        >
                          <p>
                            <span
                              onClick={(e) => {
                                dispahch(api.list_like(item.id));
                                e.stopPropagation();
                              }}
                            >
                              {item.flag ? (
                                <HeartFilled style={{ color: "pink" }} />
                              ) : (
                                <HeartOutlined />
                              )}
                              {item.likes}·
                            </span>
                            <span>
                              <EyeOutlined />
                              {item.views}·
                            </span>
                            <span
                              className="fx"
                              onClick={(e) => {
                                setIsModalOpen(true);
                                SetItem(item);
                                e.stopPropagation();
                              }}
                            >
                              <ApiOutlined />
                              share
                            </span>
                          </p>
                        </div>
                      </div>
                    </div>
                  );
                })
                :<Empty/>
              }
              </div>
            </div>
          </div>
          {/* 封装的弹窗组件 */}
          <Alert
            isModalOpen={isModalOpen}
            close={close}
            alertitem={alertitem}
          />
          {/* 首页右侧内容 */}
          <div className="contentright">
            <FilterRight />
          </div>
        </div>
      ) : (
        <div className="contenterout">
          <Ipad categoryList={categoryList}></Ipad>
        </div>
      )}

      {/* <BackTop/> */}
      <BackTop />
    </div>
  );
};

export default Index;
