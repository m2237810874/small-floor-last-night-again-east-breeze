import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import "./index.scss";
import * as api from "../../../api/index";
import { useNavigate } from "react-router-dom";
import Right from "../../../component/right";
import style from "../../../style/style.module.css";
import { useWinSize } from "../../../hooks/WinSize";
import moment from "moment";

const Index = () => {
  const dispahch = useDispatch();
  const navigate = useNavigate();
  const size = useWinSize();
  useEffect(() => {
    dispahch(api.get_list());
  }, [dispahch]);
  const List = useSelector((state) => state.Reducer.List);
  console.log(List);
  return (
    <div className="index">
      {size.width >= 700 ? (
        <div className="archpc">
          <div
            className="index_left"
            style={{ width: "71%", marginLeft: "3%" }}
          >
            <div className="index_nav" id={style.nav}>
              <h4 style={{ color: "rgb(245, 97, 195)", fontSize: "24px" }}>
                archives
              </h4>
              <p style={{ fontSize: "15px" }}>
                total
                <span
                  style={{
                    color: "rgb(245, 97, 195)",
                    margin: "5px",
                  }}
                >
                  3
                </span>
                piece
              </p>
            </div>
          <div className="index_main" id={style.nav}>
            <h4 style={{ padding: "10px", fontSize: "22px" }}  id={style.hcolor}>2022</h4>
            <p style={{ padding: "0px 10px 10px 20px", color: "#a7a7a7" ,fontSize:'16px'}}>
              September
            </p>
            <ul>
              {List.map((item, index) => {
                return (
                  <li className="list"  id={style.nav} key={index} onClick={()=>navigate('/home/detail/'+item.id)}>
                    <span style={{ fontSize: "13px" }}>{moment(item.createAt).format('MM-DD')}</span>
                    <p style={{ display: "inline-block",zIndex:'999', marginLeft: "20px",fontWeight:'bold',fontSize:'17px' }}>
                      {item.title}
                    </p>
                  </li>
                );
              })}
            </ul>
          </div>
          </div>
          <div className="index_right">
          <Right></Right>
          </div>
        </div>
      ) : (
        <div className="archpc">
          <div className="index_left" style={{ width: "100%" }}>
            <div className="index_nav" id={style.nav}>
              <h4 style={{ color: "rgb(245, 97, 195)", fontSize: "24px" }}>
                archives
              </h4>
              <p style={{ fontSize: "15px" }}>
                total
                <span
                  style={{
                    color: "rgb(245, 97, 195)",
                    margin: "5px",
                  }}
                >
                  3
                </span>
                piece
              </p>
            </div>
            <div className="index_main" id={style.nav}>
              <h4
                style={{ padding: "10px", fontSize: "22px" }}
                id={style.hcolor}
              >
                2022
              </h4>
              <p
                style={{
                  padding: "0px 10px 10px 20px",
                  color: "#a7a7a7",
                  fontSize: "16px",
                }}
              >
                September
              </p>
              <ul>
                {List.map((item, index) => {
                  return (
                    <li
                      className="list"
                      id={style.nav}
                      key={index}
                      onClick={() => navigate("/home/detail/" + item.id)}
                    >
                      <span style={{ fontSize: "13px" }}>{item.createAt}</span>
                      <p
                        style={{
                          display: "inline-block",
                          zIndex: "999",
                          marginLeft: "20px",
                          fontWeight: "bold",
                          fontSize: "17px",
                        }}
                      >
                        {item.title}
                      </p>
                    </li>
                  );
                })}
              </ul>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};
export default Index;
