import React, { useState} from 'react'
import { useSelector } from 'react-redux'
import { useNavigate, useParams } from 'react-router-dom'
import './index.scss'
import * as api from '../../../api/index'
import { useDispatch } from 'react-redux'
import Alert from '../../../component/Alert/Alert';
import { HeartOutlined , EyeOutlined , ApiOutlined} from '@ant-design/icons';
import style from '../../../style/style.module.css'
import FilterRight from '../../../component/FilterRight/FilterRight'
import moment from 'moment'
import { Empty } from 'antd'


const Index = () => {
  const params=useParams()
  const dispatch=useDispatch()
  const navigate=useNavigate()
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [alertitem,SetItem]=useState({})
  const active=useSelector((state)=>state.Reducer.active)
  const [actives,setactive]=useState(active)
  const close=(flag)=>{
    setIsModalOpen(flag)
  }
  const categoryList=useSelector((state)=>state.Reducer.categoryList)
  const filterList=useSelector((state)=>state.Reducer.filterList)
  return (
    <div className='category'>
        <div className="category_left">
            <div className="category_left_top"  id={style.nav}>
              <h4 style={{ color: "rgb(245, 97, 195)", fontSize: "24px" }}>
                {params.id}
              </h4>
            </div>
            <div className="category_left_bottom"  id={style.nav}>
              <div className="category_left_bottom_list">
                {
                  categoryList.map((item,index)=>{
                    return <p key={index} onClick={()=>{
                      if(item!=='all'){
                        dispatch(api.filter_list(item))
                        setactive(index)
                        navigate('/home/category/'+item)
                      }
                      else{
                        navigate('/home/articles')
                      }
                    }}
                      className={actives===index?'active':''}
                    >
                      {item}
                    </p>
                  })
                }
              </div>
              <div className="category_left_bottom_filter_list">
              {
            filterList.length?filterList.map((item,index)=>{
              return <div key={index} className="filter_list">
                        <div className="filter_list_item">
                          <h4  id={style.hcolor}>
                        {
                              item.title    
                            }
                          </h4>
                          <p>
                            <span>
                            大约&ensp;{moment(item.createAt).startOf('hour').fromNow()}
                            </span>
                            <span>
                            {item.category?item.category.label:''}
                            </span>
                          </p>

                        </div>
                        <div className="filter_list_con">
                          <div className="filter_list_con_left">
                              {item.cover?<img width='100px' height='100px' src={item.cover} alt="" />:''}
                          </div>
                          <div className="filter_list_con_right">
                              <p>{item.desc}</p>
                              <p>
                                <span>
                                  <HeartOutlined />
                                  {item.like}
                                  ·
                                </span>
                                <span>
                                  <EyeOutlined />
                                  {item.watch}
                                  ·
                                </span>
                                <span className='fx' onClick={()=>{
                                  setIsModalOpen(true)
                                  SetItem(item)
                                }}>
                                  <ApiOutlined />
                                  share
                                </span>
                              </p>
                          </div>
                        </div>
                    </div>
            })
            :<Empty/>
          }
              </div>
            </div>
        </div>
        <div className="category_right">
          <FilterRight></FilterRight>
        </div>
        <Alert isModalOpen={isModalOpen} close={close} alertitem={alertitem}/>
    </div>
  )
}

export default Index