import React from "react";
import "./index.scss";
import { Input } from "antd";
import * as api from '../../api/index'
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { debounce } from "lodash"
const { Search } = Input;
const Index = () => {
    const navigate=useNavigate()
    const dispahch=useDispatch()
  const onSearch = (value) => {
    dispahch(api.search_list(value))
  };
  const searchList=useSelector((state)=>state.Reducer.searchList)
  return (
    <div className="search">
      <div className="search_box">
        <h1>searchArticle</h1>
        <Search
          placeholder="input search text"
          allowClear
          enterButton="Search"
          size="large"
          onSearch={debounce(onSearch,3000)}
        />
        <div>
            {
                searchList.map((item,index)=>{
                    return <p key={index} onClick={()=>navigate('/home/detail/'+item.id)}>
                        {item.title}
                    </p>
                })
            }
        </div>
      </div>
    </div>
  );
};

export default Index;
