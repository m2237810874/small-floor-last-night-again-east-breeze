import { Button, Empty } from 'antd';
import React, { useEffect } from "react";
import "./index.scss";
import * as api from '../../api/index'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import style from '../../style/style.module.css'
import moment from 'moment';

const FilterRight = () => {
    const dispatch=useDispatch()
    useEffect(()=>{
        dispatch(api.set_list())
    },[])
    const list=useSelector((state)=>state.Reducer.List)
    const taglist=useSelector((state)=>state.Reducer.setlist)
    const num=useSelector((state)=>state.Reducer.num)
    console.log(num);
    const navigate=useNavigate()
    return (
        <div className="FilterRight">
            <div className="onebox" id={style.nav}>
                <h3  style={{
                    paddingLeft:'10px'
                }} id={style.nav}>recommendToReading</h3>
                <ul>
                    {
                        list.length?list.map((item,index)=>{
                            return  <li key={index} className='list' onClick={()=>{
                                        navigate('/home/detail/'+item.id)
                                    }}>
                                        <p> {item.title}</p>
                                        <p style={{marginLeft:'4px'}}>大约&ensp;{moment(item.createAt).startOf('hour').fromNow()}</p>
                                    </li>
                        })
                        :<Empty/>
                    }
                </ul>
            </div>
            <div className="twobox" id={style.nav}>
                <h3 style={{
                    paddingLeft:'10px'
                }} id={style.nav}>tagTitle</h3>
                <div className="tags" id={style.nav}>
                    {
                        taglist.map((item,index)=>{
                            return <Button key={index}
                                    id={style.nav}
                                    onClick={()=>{
                                        dispatch(api.tag_filter_list(item.label))
                                        navigate('/home/tag/'+item.label)
                                        
                                    }}
                                    >
                                        {
                                            item.label
                                        }
                                        <span style={{
                                            marginLeft:'5px'
                                        }}>[{
                                            num
                                        }]</span>
                                        
                                    </Button>
                        })
                    }
                </div>
                    
            </div>
        </div>
    );
};

export default FilterRight;
