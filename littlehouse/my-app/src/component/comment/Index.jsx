import React from 'react'
import { Avatar, Comment } from "antd";
import * as api from '../../api'
import moment from 'moment';
import { useDispatch } from 'react-redux';
const Index = (props) => {
  const {detailList,pageList,setValue1,value1,setValue2,value2}=props
  const dispatch=useDispatch()
  const ExampleComment = () =>
      pageList.map((item, index) => {
        return (
          <Comment
            key={index}
            actions={[
              <>
                <span>
                  <span>
                    Chrome 103.0.0.0 Blink 103.0.0.0 Windows 10 · 大约 {moment(item.createAt).startOf('day').fromNow()}
                  </span>
                </span>
                <span
                  key="comment-nested-reply-to"
                  onClick={() => {
                    dispatch(api.change_flag(item.id));
                  }}
                >
                  回复
                </span>
                ,
              </>,
            ]}
            author={<a>{item.name}</a>}
            avatar={
              <Avatar src="https://joeschmoe.io/api/v1/random" alt="Han Solo" />
            }
            content={
              <p
                style={{
                  padding: "12px 0px 12px 0",
                }}
              >
                {item.content}
              </p>
            }
          >
            {item.flag ? (
              <div className="flag">
                <div className="detail_left_bottom_Commentable">
                  <textarea
                    placeholder="请输入评论内容（支持 Markdown）"
                    class="ant-input"
                    style={{
                      height: "142px",
                      minHeight: "142px",
                      maxHeight: "274px",
                      overflowY: "hidden",
                      resize: "none",
                    }}
                    onChange={(e)=>{
                      setValue1(e.target.value)
                    }}
                  ></textarea>
                </div>
                <div className="detail_left_bottom_box">
                  <div className="detail_left_bottom_box_svg">
                    <span>
                      <svg viewBox="0 0 1024 1024" width="18px" height="18px">
                        <path
                          d="M288.92672 400.45568c0 30.80192 24.97024 55.77216 55.77216 55.77216s55.77216-24.97024 55.77216-55.77216c0-30.7968-24.97024-55.76704-55.77216-55.76704s-55.77216 24.97024-55.77216 55.76704z m334.60224 0c0 30.80192 24.97024 55.77216 55.77216 55.77216s55.77216-24.97024 55.77216-55.77216c0-30.7968-24.97024-55.76704-55.77216-55.76704s-55.77216 24.97024-55.77216 55.76704z m-111.5392 362.4704c-78.05952 0-156.13952-39.08096-200.75008-100.3776-16.77312-22.31296-27.84256-50.15552-39.08096-72.45824-5.53472-16.77312 5.5296-33.4592 16.77312-39.08096 16.77312-5.53472 27.84256 5.53472 33.46432 16.768 5.53472 22.30784 16.77312 39.08608 27.84256 55.77728 44.61568 55.76704 100.38272 83.69664 161.664 83.69664 61.30176 0 122.7008-27.84256 156.16-78.07488 11.15136-16.77824 22.30784-38.99904 27.84256-55.77728 5.62176-16.768 22.30784-22.30272 33.4592-16.768 16.768 5.53472 22.30784 22.30272 16.768 33.4592-5.61152 27.84256-22.2976 50.14528-39.08096 72.45824-38.912 61.37856-116.98176 100.3776-195.06176 100.3776z m0 194.51392C268.4928 957.44 66.56 755.52256 66.56 511.99488 66.56 268.48256 268.4928 66.56 511.98976 66.56 755.50208 66.56 957.44 268.48256 957.44 511.99488 957.44 755.52256 755.50208 957.44 511.98976 957.44z m0-831.45728c-213.78048 0-386.00192 172.21632-386.00192 386.01216 0 213.8112 172.22144 386.0224 386.00192 386.0224 213.80096 0 386.0224-172.2112 386.0224-386.0224 0-213.79584-172.22144-386.01216-386.0224-386.01216z"
                          fill="currentColor"
                        ></path>
                      </svg>
                      <span>表情</span>
                    </span>
                  </div>
                  <div>
                    <button>
                      <span onClick={()=>{
                        dispatch(api.add_commendListChildren(value1,item.id))
                      }}>发布</span>
                    </button>
                  </div>
                </div>
              </div>
            ) : (
              ""
            )}
            {item.children ? (
              <div>
                {item.children.map((v, i) => {
                  return (
                    <Comment
                      key={i}
                      actions={[
                        <>
                          <span>
                            Chrome 103.0.0.0 Blink 103.0.0.0 Windows 10 · 大约 18
                            小时前
                          </span>
                          <span
                            key="comment-nested-reply-to"
                            onClick={() => {
                              dispatch(api.children_flag(item.id, v.id));
                            }}
                          >
                            回复
                          </span>
                        </>,
                      ]}
                      author={<a>{v.name}</a>}
                      avatar={
                        <Avatar
                          src="https://joeschmoe.io/api/v1/random"
                          alt="Han Solo"
                        />
                      }
                      content={
                        <p
                          style={{
                            padding: "12px 0px 12px 0",
                          }}
                        >
                          {v.content}
                        </p>
                      }
                    >
                      {v.flag ? (
                        <div className="flag">
                          <div className="detail_left_bottom_Commentable">
                            <textarea
                              placeholder="请输入评论内容（支持 Markdown）"
                              class="ant-input"
                              style={{
                                height: "142px",
                                minHeight: "142px",
                                maxHeight: "274px",
                                overflowY: "hidden",
                                resize: "none",
                              }}
                              onChange={(e)=>{
                                setValue2(e.target.value)
                              }}
                            ></textarea>
                          </div>
                          <div className="detail_left_bottom_box">
                            <div className="detail_left_bottom_box_svg">
                              <span>
                                <svg
                                  viewBox="0 0 1024 1024"
                                  width="18px"
                                  height="18px"
                                >
                                  <path
                                    d="M288.92672 400.45568c0 30.80192 24.97024 55.77216 55.77216 55.77216s55.77216-24.97024 55.77216-55.77216c0-30.7968-24.97024-55.76704-55.77216-55.76704s-55.77216 24.97024-55.77216 55.76704z m334.60224 0c0 30.80192 24.97024 55.77216 55.77216 55.77216s55.77216-24.97024 55.77216-55.77216c0-30.7968-24.97024-55.76704-55.77216-55.76704s-55.77216 24.97024-55.77216 55.76704z m-111.5392 362.4704c-78.05952 0-156.13952-39.08096-200.75008-100.3776-16.77312-22.31296-27.84256-50.15552-39.08096-72.45824-5.53472-16.77312 5.5296-33.4592 16.77312-39.08096 16.77312-5.53472 27.84256 5.53472 33.46432 16.768 5.53472 22.30784 16.77312 39.08608 27.84256 55.77728 44.61568 55.76704 100.38272 83.69664 161.664 83.69664 61.30176 0 122.7008-27.84256 156.16-78.07488 11.15136-16.77824 22.30784-38.99904 27.84256-55.77728 5.62176-16.768 22.30784-22.30272 33.4592-16.768 16.768 5.53472 22.30784 22.30272 16.768 33.4592-5.61152 27.84256-22.2976 50.14528-39.08096 72.45824-38.912 61.37856-116.98176 100.3776-195.06176 100.3776z m0 194.51392C268.4928 957.44 66.56 755.52256 66.56 511.99488 66.56 268.48256 268.4928 66.56 511.98976 66.56 755.50208 66.56 957.44 268.48256 957.44 511.99488 957.44 755.52256 755.50208 957.44 511.98976 957.44z m0-831.45728c-213.78048 0-386.00192 172.21632-386.00192 386.01216 0 213.8112 172.22144 386.0224 386.00192 386.0224 213.80096 0 386.0224-172.2112 386.0224-386.0224 0-213.79584-172.22144-386.01216-386.0224-386.01216z"
                                    fill="currentColor"
                                  ></path>
                                </svg>
                                <span>表情</span>
                              </span>
                            </div>
                            <div>
                              <button>
                                <span onClick={()=>{
                                  dispatch(api.add_commend(value2,item.id))
                                }}>发布</span>
                              </button>
                            </div>
                          </div>
                        </div>
                      ) : (
                        ""
                      )}
                    </Comment>
                  );
                })}
              </div>
            ) : (
              ""
        
            )}
          </Comment>
        );
      });
    return (
    <div>
        <div className="commentable">
          {detailList.isCommentable ? <ExampleComment></ExampleComment> : ""}
        </div>
    </div>
  )
}

export default Index