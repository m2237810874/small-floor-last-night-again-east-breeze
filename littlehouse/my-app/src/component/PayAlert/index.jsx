import { Button, Modal } from 'antd';
import axios from '../../utils/request'
import React,{useState} from 'react'
import { useNavigate } from 'react-router-dom'


const Index = ({isPay,id}) => {
    const [isModalOpen,setIsModalOpen]=useState(isPay)
    const navigate=useNavigate()
    const handleOk=()=>{
        axios.get('/payment/pay').then(res=>{
            const url=res.data.result
            window.location.href=url
        })
    }
    const handleCancel=()=>{
        setIsModalOpen(false)
        navigate(-1)
    }
    return (
        <div>
            <Modal title="确认以下收费信息" open={isModalOpen} onOk={handleOk} onCancel={handleCancel}
                footer={[
                    <Button key="back" onClick={handleCancel}>
                        取消
                    </Button>,
                    <Button key="submit" type="primary" onClick={handleOk}>
                        支付
                    </Button>
                ]}>
                <p>支付金额:10</p>
            </Modal>
        </div>
    )
}

export default Index