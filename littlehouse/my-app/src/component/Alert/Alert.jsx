import { Modal } from 'antd';
import QRCode from 'qrcode.react';
import Knowledge from '../knowledge/Knowledge'
import './alert.scss'


const Alert = (props) => {
    const { isModalOpen , close , alertitem}=props

    const handleOk = () => {
        close(false)
    };
    
    const handleCancel = () => {
        close(false)
    };
    return (
        <Modal title="分享" open={isModalOpen}  onOk={handleOk} onCancel={handleCancel} footer={null}>
            <img style={{width:"465px",height:"300px"}} src={alertitem.cover} alt="" />
            <h3>{alertitem.title}</h3>
            <div className='alertbox'>
                <div className="alertLeft">
                    <QRCode
                        value="www.baidu.com"  //value参数为生成二维码的链接
                        size={100} //二维码的宽高尺寸
                        fgColor="#000000"  //二维码的颜色
                    />
                </div>
                <div className="alertRight">
                    <p>识别二维码查看文章</p>
                    <p>原文分享自ikun</p>
                </div>
            </div>
                <Knowledge alertitem={alertitem} handleOk={handleOk} isModalOpen={isModalOpen}></Knowledge>
            
            
        </Modal>
    )

}

export default Alert