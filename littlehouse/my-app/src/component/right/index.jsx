import React,{useEffect} from 'react'
import {useSelector,useDispatch} from "react-redux"
import './index.scss'
import * as api from '../../api/index'
import { useNavigate  } from 'react-router-dom'
import style from '../../style/style.module.css'
import moment from 'moment'
const Index = () => {
    const dispatch=useDispatch()
    const navigate=useNavigate()
    useEffect(()=>{
        dispatch(api.get_list())
        dispatch(api.get_rightlist())
    },[dispatch])
    const list=useSelector((state)=>state.Reducer.List)
    const rightlist=useSelector((state)=>state.Reducer.rightList)
  return (
    <div className='right'>

        <div className='first_box'  id={style.nav}>
            <h3 style={{
                height:'50px',
                display:'flex',
                alignItems:'center',
                paddingLeft:'10px',
                fontWeight:'bold'
            }} id={style.nav}>recommendToReading</h3>
            <div>
               <ul>
               {
                    list.map((item,index)=>{
                        return <li key={index} className='list_li'onClick={()=>navigate('/home/detail/'+item.id)}>


                           <p> {item.title}</p>
                           <p> 大约&ensp;{moment(item.createAt).startOf('hour').fromNow()} </p>
                        </li>
                    })
                }
               </ul>
            </div>
        </div>
        <div className='second_box' id={style.nav}>
            <h3 style={{
                height:'50px',
                display:'flex',
                alignItems:'center',
                paddingLeft:'10px',
                fontWeight:'bold'
            }} id={style.nav}>categoryTitle</h3>
            <ul id={style.nav}>
                {
                    rightlist.map((item,index)=>{
                        return <li key={index} className="right_list" onClick={()=>{
                            navigate('/home/category/'+item.label)
                            dispatch(api.begin_filter_list(item, index+1))
                        }}>
                            <div>{item.label}</div>
                            <p>Total<span style={{
                                margin:'0px 5px'
                            }}>{item.articleCount}</span>Count</p>
                        </li>
                    })
                }
            </ul>
        </div>
    </div>
  )
}

export default Index