import { useTranslation } from 'react-i18next'
import React, { useState , useEffect } from 'react'
import { NavLink } from 'react-router-dom'
import i18n from 'i18next';
import {  Popup } from 'react-vant'
import { WapNav , Cross } from '@react-vant/icons';
import './index.scss'
import style from '../../style/style.module.css'
import { useSelector } from 'react-redux'

const isLight=()=>{
    const currentDate=new Date();
    return currentDate.getDate() > 6 && currentDate.getHours() < 19;
}

const MobileNav = () => {
    const [flag,setFlag]=useState(false)
    const [cut,setcut]=useState('zh')
    const [theme,setTheme]=useState(()=>(isLight()?'白':'黑'));
    const { t } = useTranslation()//国际化
    const logo=useSelector((store)=>store.Reducer.logoimg)
    const conversion= (val) => {
        val==='zh'?setcut('en'):setcut('zh')
       i18n.changeLanguage(val); // val入参值为'en'或'zh'
     };
    useEffect(()=>{
        document.documentElement.className = theme ==="白" ? "dark" : "lint"                    
    },[theme])
    return (
        <div className='MobileNav' id={style.nav}>
                <div className="headimg">
                    <img src={logo} alt="" />
                </div>
                <div onClick={()=>{
                    setFlag(!flag)
                }} className="all">
                    {
                        flag?<Cross fontSize="40px" color='rgb(255, 0, 195)' />:<WapNav fontSize="40px" color='rgb(255, 0, 195)' />
                    }
                </div>
                <Popup
                    visible={flag === true}
                    style={{ height: '30%', top:'60px'}}
                    position='top'
                    
                >
                    {
                        <ul className='mobileul' id={style.nav}>
                            <li><NavLink className={style.dark_min} to={"/home/articles"}>{t('header.Articles')}</NavLink></li>
                            <li><NavLink className={style.dark_min} to={"/home/archives"}>{t('header.pigeonhole')}</NavLink></li>
                            <li><NavLink className={style.dark_min} to={"/home/knowledgebooks"}>{t('header.Knowledge')}</NavLink></li>
                            <li>
                                <span onClick={()=>conversion(cut)}>
                                    <svg viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" width="30px" height="30px"><path d="M547.797333 638.208l-104.405333-103.168 1.237333-1.28a720.170667 720.170667 0 0 0 152.490667-268.373333h120.448V183.082667h-287.744V100.906667H347.605333v82.218666H59.818667V265.386667h459.178666a648.234667 648.234667 0 0 1-130.304 219.946666 643.242667 643.242667 0 0 1-94.976-137.728H211.541333a722.048 722.048 0 0 0 122.453334 187.434667l-209.194667 206.378667 58.368 58.368 205.525333-205.525334 127.872 127.829334 31.232-83.84m231.424-208.426667h-82.218666l-184.96 493.312h82.218666l46.037334-123.306667h195.242666l46.464 123.306667h82.218667l-185.002667-493.312m-107.690666 287.744l66.56-178.005333 66.602666 178.005333z" fill="currentColor"></path></svg>
                                </span>
                            </li>
                            <li>
                                <span onClick={()=>{
                                    setTheme(theme==='白'?'黑':'白')
                                }}>{theme==='白'?
                                  <svg t="1663380392330" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="4516" width="30" height="30"><path d="M501.48 493.55m-233.03 0a233.03 233.03 0 1 0 466.06 0 233.03 233.03 0 1 0-466.06 0Z" fill="#F9C626" p-id="4517"></path><path d="M501.52 185.35H478.9c-8.28 0-15-6.72-15-15V87.59c0-8.28 6.72-15 15-15h22.62c8.28 0 15 6.72 15 15v82.76c0 8.28-6.72 15-15 15zM281.37 262.76l-16 16c-5.86 5.86-15.36 5.86-21.21 0l-58.52-58.52c-5.86-5.86-5.86-15.36 0-21.21l16-16c5.86-5.86 15.36-5.86 21.21 0l58.52 58.52c5.86 5.86 5.86 15.35 0 21.21zM185.76 478.48v22.62c0 8.28-6.72 15-15 15H88c-8.28 0-15-6.72-15-15v-22.62c0-8.28 6.72-15 15-15h82.76c8.28 0 15 6.72 15 15zM270.69 698.63l16 16c5.86 5.86 5.86 15.36 0 21.21l-58.52 58.52c-5.86 5.86-15.36 5.86-21.21 0l-16-16c-5.86-5.86-5.86-15.36 0-21.21l58.52-58.52c5.85-5.86 15.35-5.86 21.21 0zM486.41 794.24h22.62c8.28 0 15 6.72 15 15V892c0 8.28-6.72 15-15 15h-22.62c-8.28 0-15-6.72-15-15v-82.76c0-8.28 6.72-15 15-15zM706.56 709.31l16-16c5.86-5.86 15.36-5.86 21.21 0l58.52 58.52c5.86 5.86 5.86 15.36 0 21.21l-16 16c-5.86 5.86-15.36 5.86-21.21 0l-58.52-58.52c-5.86-5.85-5.86-15.35 0-21.21zM802.17 493.59v-22.62c0-8.28 6.72-15 15-15h82.76c8.28 0 15 6.72 15 15v22.62c0 8.28-6.72 15-15 15h-82.76c-8.28 0-15-6.72-15-15zM717.24 273.44l-16-16c-5.86-5.86-5.86-15.36 0-21.21l58.52-58.52c5.86-5.86 15.36-5.86 21.21 0l16 16c5.86 5.86 5.86 15.36 0 21.21l-58.52 58.52c-5.86 5.86-15.35 5.86-21.21 0z" fill="#F9C626" p-id="4518"></path></svg>
                                  :
                                  <svg t="1663380408451" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="5597" width="30" height="30"><path d="M529.611373 1023.38565c-146.112965 0-270.826063-51.707812-374.344078-155.225827C51.74928 764.641808 0.041469 639.826318 0.041469 493.815745c0-105.053891 29.693595-202.326012 88.978393-292.22593 59.38719-89.797526 137.000103-155.942569 232.83874-198.63991 6.041111-4.607627 12.184613-3.788493 18.225724 2.252618 7.576986 4.607627 9.931996 11.365479 6.860244 20.580733C322.677735 83.736961 310.493122 142.202626 310.493122 201.589815c0 135.464227 48.328885 251.474031 144.986656 348.131801 96.657771 96.657771 212.667574 144.986656 348.131801 144.986656 74.541162 0 139.252721-11.365479 194.032283-34.19883C1003.684974 655.799424 1009.726084 656.618558 1015.767195 662.659669c7.576986 4.607627 9.931996 11.365479 6.860244 20.580733C983.104241 786.758417 918.802249 869.286132 829.721465 930.925939 740.743072 992.565746 640.706375 1023.38565 529.611373 1023.38565z" p-id="5598"></path></svg>
                                  }
                                </span>
                            </li>
                        </ul>
                    }
                </Popup>
        </div>
    )
}

export default MobileNav