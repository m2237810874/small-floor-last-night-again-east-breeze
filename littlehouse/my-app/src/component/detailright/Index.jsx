import moment from 'moment'
import React from 'react'
import { useDispatch } from 'react-redux'
import style from '../../style/style.module.css'
import * as api from '../../api/index'
const Index = (props) => {
    const {navigate,active,List,tocList,setActive,arr}=props
    const dispatch=useDispatch()
  return (
    <div>
        <div className="detail_right">
          <div className="detail_right_head">
            <h3
              style={{
                height: "50px",
                display: "flex",
                alignItems: "center",
                paddingLeft: "10px",
                fontWeight: "bold",
              }}
              id={style.nav}
            >
              recommendToReading
            </h3>
            <div id={style.nav}>
              <ul>
                {List.map((item, index) => {
                  return (
                    <li
                      key={index}
                      className="list_li"
                      onClick={() => {
                        navigate("/home/detail/" + item.id)
                        dispatch(api.goto_detail(item.id))
                      }}

                    >
                      <p> {item.title}</p>
                      <p>大约&ensp;{moment(item.createAt).startOf('hour').fromNow()}</p>
                    </li>
                  );
                })}
              </ul>
            </div>
          </div>
          <div className="detail_right_bottom">
            <h3
              style={{
                height: "50px",
                display: "flex",
                alignItems: "center",
                paddingLeft: "10px",
                fontWeight: "bold",
                fontSize: "15px",
              }}
            >
              目录
            </h3>
            <div
              style={{
                paddingLeft: "10px",
              }}
            >
              {tocList.map((item, index) => {
                return (
                  <div
                    className={active === index ? "active" : "toclist"}
                    value={active}
                    key={index}
                    onClick={() => {
                      setActive(index);
                      document.documentElement.scrollTop =
                        arr[index].offsetTop + 80;
                    }}
                  >
                    {item.text}
                  </div>
                );
              })}
            </div>
          </div>
        </div>
    </div>
  )
}

export default Index