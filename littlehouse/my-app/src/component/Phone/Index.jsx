import React,  {useState,useEffect} from 'react'
import { useSelector,useDispatch } from 'react-redux'
import * as api from '../../api/index'
import { useNavigate } from "react-router-dom";
import Alert from "../Alert/Alert";
import { HeartOutlined, EyeOutlined, ApiOutlined, HeartFilled } from "@ant-design/icons";
import "./index.scss"
const Index = () => {
  const dispatch=useDispatch();
  const navigate=useNavigate()
  const [active,SetActive]=useState(0)
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [alertitem, SetItem] = useState({});
  useEffect(()=>{
    dispatch(api.get_list());
    dispatch(api.get_rightlist)
  },[dispatch])
   
    const {List,categoryList } = useSelector(({ Reducer }) => {
      return {
        ...Reducer,
      };
    });
    const close = (flag) => {
      setIsModalOpen(flag);
    };

  return (
    <div className='tab'>
        <div className='tabnav'>
           <ul>
             {
              categoryList.length?categoryList.map((item,index)=>{
                return (
                  <li key={index} className={active===index ?"active":'navtop'}
                  onClick={()=>{
                    SetActive(index);
                    if(item!=='all'){
                      //跳转路由
                      navigate('/home/category/'+item)
                      // 派发仓库
                      dispatch(api.begin_filter_list(item,index))
                    }
                  }}>
                    {item}
                  </li>
                )
              }):'暂无数据'
             }
           </ul>
        </div>
        <div className='tabcon'>
          {List.map((item, index) => {
            return (
              <div key={index} className="item" onClick={()=>navigate('/home/detail/'+item.id)}>
                <div className="itemhead">
                  <h2>{item.title}</h2>
                  <p>
                    <span>大约...分钟前</span>
                    <span>{item.category?item.category.label:''}</span>
                  </p>
                </div>
                <div className="itemcon">
                  <div className="itemlefy">
                    {item.cover ? <img src={item.cover} alt="" /> : ""}
                  </div>
                </div>
                <div className="ltemright">
                    <p>{item.desc}</p>
                    <p>
                    <span onClick={(e)=>{
                       dispatch(api.list_like(item.id));
                       e.stopPropagation();
                    }}>
                            {item.flag ? (
                              <HeartFilled style={{ color: "pink" }} />
                            ) : (
                              <HeartOutlined />
                            )}
                            {item.likes}
                          </span><span>.</span>
                      <span>
                        <EyeOutlined />
                        {item.views}
                      </span><span>.</span>
                      <span  className="fx"
                        onClick={(e) => {
                          setIsModalOpen(true);
                          SetItem(item);
                          e.stopPropagation()
                        }}>
                        <ApiOutlined />
                        share
                      </span>
                    </p>
                  </div>
              </div>
            );
          })}
        </div>
         {/* 封装的弹窗组件 */}
      <Alert isModalOpen={isModalOpen} close={close} alertitem={alertitem} />
    </div>
  )
}

export default Index