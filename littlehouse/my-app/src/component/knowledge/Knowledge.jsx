import { Button } from 'antd';
import React,{useState,useEffect,useRef} from 'react'
import useCanvas from '../../hooks/useCanvas'
import './Knowindex.scss'


const Knowledge = ({alertitem,handleOk}) => {
    // const [flag,setFlag]=useState(false)
    const [adata,setData]=useState({})
    const shareBtn=()=>{
        // setFlag(!flag);
    };
    useEffect(()=>{
        setData(alertitem)
    },[adata])
    const canvasDom=useRef(null)
    const canvasUrl=useCanvas(adata,canvasDom)
    const down=()=>{
        handleOk()
    }
    return (
        <div className='Knowindex'>
            
            {/* 生成海报 */}
            <canvas style={{display:'none'}} ref={canvasDom}>你的浏览器不支持canvas</canvas>
            {/* {
                flag&&(
                <div>
                    <img src={canvasUrl} alt="" />
                    
                </div>
            )} */}
            <div className="footers">
                <Button onClick={shareBtn} type="primary" danger>
                    <a href={canvasUrl} download="canvas.png">
                        下载
                    </a>
                </Button>
                <Button type="primary" onClick={down}>取消</Button>
            </div>
        </div>
    )
}

export default Knowledge