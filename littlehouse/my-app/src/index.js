import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import RouterViews from './router/RouteViews';
import './i18n/config'; // 引用配置文件
import store from './store';
import { Provider } from 'react-redux'
const root = ReactDOM.createRoot(document.getElementById('root'));

root.render(
  <Provider store={store}>
    <RouterViews/>
  </Provider>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
