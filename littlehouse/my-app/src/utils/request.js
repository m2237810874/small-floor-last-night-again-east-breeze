import axios from 'axios'

axios.create({
    timeout:3000

})


// 添加请求拦截器
axios.interceptors.request.use(function(config) {
    // 在发送请求之前做些什么
    return config;
}, function(error) {
    // 对请求错误做些什么
    return Promise.reject(error);
});

// 添加响应拦截器
axios.interceptors.response.use(function(response) {
    // 对响应数据做点什么
    return response;
}, function(error) {
    //请求超时处理


    if (error.code ==='ECONNABORT') {
        console.log('请求超时');
    }
    //不同错误响应的处理
    let status = error.response.status //400  401 402 403 404  500 505
    if (status === 400) {
        console.log("错误请求")
    } else if (status === 401) {
        console.log("未授权，请重新登录")
    } else if (status === 403) {
        console.log('拒绝访问');
    } else if (status === 404) {
        console.log('请求错误,未找到该资源');
    } else if (status === 500) {
        console.log('服务端出错');
    }

    // 对响应错误做点什么
    return Promise.reject(error);
});


export default axios

