const createProxyMiddleware =require("http-proxy-middleware")

module.exports=(app)=>{
    app.use(
        createProxyMiddleware('/api',{
            target:"https://creationapi.shbwyz.com",
            changeOrigin:true,
            pathRewrite:{
                '^/api':''
            }
        })
    )
}