const Koa = require('koa');
const Router = require('koa-router');

const app = new Koa();
const router = new Router()

//引入sdk

const AlipaySDK = require('alipay-sdk').default;
const AlipayFormData = require('alipay-sdk/lib/form').default;

// 创建AlipaySDK
const alipaySdk = new AlipaySDK({
    appId: '2021000121672025', // 开放平台上创建应用时生成的 appId
    signType: 'RSA2', // 签名算法,默认 RSA2
    gateway: 'https://openapi.alipaydev.com/gateway.do', // 支付宝网关地址 ，沙箱环境下使用时需要修改
    // 支付宝公钥，需要对结果验签时候必填
    alipayPublicKey:
        'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApMhLcmdDLt3piGZOeauzWUDRg0ITQ7AEbqR0DKAlKm0mZrD92Y+sGH6b9f0l068PC0H4sgYJOjkLpaqrGZX1b1NRHpmP00DsQK0rVTXuBNwyM7k4aHIS2swGcNkoBwIc3Px/HAbJ76b7OetT+Y2qa1ElvdnPpsgOhDVrNo6bh1mn7bZHboL2FyukgTmim1TjBm9zXrIoiFtraLTXklXyt2cKQd367SBX084VYnQvt1ci9w2YkOeMhWQ1kmruUD+P77DBLBHO/BJ8iGL/EwB/3g47M1/YgdA2zKJObVvDKfX51YXuODbWlw8/qY/bgpVrcJVK/+VuTh+Emm5SU7yC6QIDAQAB', 
    // 应用私钥字符串
     privateKey:
        'MIIEowIBAAKCAQEApMhLcmdDLt3piGZOeauzWUDRg0ITQ7AEbqR0DKAlKm0mZrD92Y+sGH6b9f0l068PC0H4sgYJOjkLpaqrGZX1b1NRHpmP00DsQK0rVTXuBNwyM7k4aHIS2swGcNkoBwIc3Px/HAbJ76b7OetT+Y2qa1ElvdnPpsgOhDVrNo6bh1mn7bZHboL2FyukgTmim1TjBm9zXrIoiFtraLTXklXyt2cKQd367SBX084VYnQvt1ci9w2YkOeMhWQ1kmruUD+P77DBLBHO/BJ8iGL/EwB/3g47M1/YgdA2zKJObVvDKfX51YXuODbWlw8/qY/bgpVrcJVK/+VuTh+Emm5SU7yC6QIDAQABAoIBAFi1XzByeM91NbBxaxPDJGULKP8hhh9tVaV+sN7PI9LoOcEUzUPfPYVeO3W81y1TU6T62fh0mWkXpraD32KBwUQyNTxk7PGMROM4llhkFQPWysvfjYdkydOx/FGqBgtnzsQExiT/Xpy+ZwayMl3pLvJhGBFRfaFKl84Ct/oB4aaF8hbttktrttO7mJoqEnMfd2rI6pn3fu7C9ua8IMT7/DcHwX+MFkvG3+qx0nqkDVtEb0baKelNGbRzMiQXZw2sFkIUmu+vOyYFPVHIg5sQo09CP1ZpSW8CjErY4IugGKFi4AI3CMOx4F6gvQrQXbBDAgO3qG56atkcVuKMyDZdlZUCgYEA/5szpdv7wQ1oK3+IBgBjJsg1SnhmIDsnyLOVLo+gY24iz/+y83d1qzMrMri3UHOkcgyVYeos9nNEmvH4rH2yhuMSViAKH4VbIESAgs9iGleR+SfTEm/+Zpg9gQLIylkx3zOtqseV/9yLPWYDUMNzcWFlVc8GjVf950DkVd7kcYcCgYEApQlGz5FAQ19wkBb8QARS/50Nx3HYIfFIdnv0BM5JQxN0vNBjqAWlmOp9c0W5XDfdbNZ2JbDJHrg40lnZNGI9toK/e9e0oHiQUDDjvsES3/qVx8a29EJkPrufh6VbmWyZobH/E3rEfpQjQmN+jHgP5mecmA8LSmFnMVg00Qy+RA8CgYANa6e1kKxSQZLznbmVnxWVprNtmo66KID5P7yYekAQmGrJgwJxWBdcis481ogBeGDtAA4j2vjLY9f5TrUg0WS7QlbnIL2HVCgptI8Ozza2bVyjuVySvkCcQGgSSwQh47cDoiH/MoWErp689Ys9BVYdYODCNaGBcFpLdxPGEnLqJwKBgD/0gVlsy3+QW7mq/qP8SQG/phhVONGmhFNOI7k9C80PnByTkRsK3SqOLIFgUr229Hr5/R0MDlR2RabWpMKVE59HudbPN7M8JYi9+XPxtFPR8vtROZMcq2ZYJTFW1lF1cW/k35l36O+4jpVqox7iZB8ApgGTSufPKUNJEGa2zoDnAoGBAKGBuTmuX5rueUmrKtqiMG7Y8dYzAs+7xDt2f0kdZJAbCrjCO8HPtx8hsREhS0TMl+mfOG0QvnIVi0jBccZEdncJyepofVhnaFgIlvujzQYgbsdJJaU6BqTq09uBWtPrMcZ053pzAg14tHYMeGuK4qLIsIodBZV8DGKcGh0IMjS8',
});

const MyOutTradeNo=Math.random().toString().slice(3,21)

//支付接口
router.get('/pay', async ctx => {
    const formData = new AlipayFormData();
    formData.setMethod('get');
    formData.addField('notifyUrl', 'https://www.baidu.com');
    formData.addField('bizContent', {
        outTradeNo: MyOutTradeNo, // 商户订单号,64个字符以内、可包含字母、数字、下划线,且不能重复
        productCode: 'FAST_INSTANT_TRADE_PAY', // 销售产品码，与支付宝签约的产品码名称,仅支持FAST_INSTANT_TRADE_PAY
        totalAmount: '0.01', // 订单总金额，单位为元，精确到小数点后两位
        subject: '商品', // 订单标题
        body: '商品详情', // 订单描述
    });
    formData.addField('returnUrl', 'http://localhost:3000/home/detail/bee684eb-a1b9-454a-8dea-0aef8b791e90');
    const result = await alipaySdk.exec(
        // result 为可以跳转到支付链接的 url
        'alipay.trade.page.pay', // 统一收单下单并支付页面接口
        {}, // api 请求的参数（包含“公共请求参数”和“业务参数”）
        { formData: formData }
    );
    ctx.body = {
        code: 1,
        result,
    };
});

app.use(router.routes(), router.allowedMethods());
app.listen(9000,() => {
    console.log(`service is running at 9000`)
});